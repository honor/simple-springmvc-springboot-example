package com.example.springmvc.dataloader;

import com.example.springmvc.Repositories.ProductRepository;
import com.example.springmvc.models.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/*
* This class is used to pre populate the database. No business logic in here.
* */
@Component
public class DataLoader implements CommandLineRunner {

    private ProductRepository productRepository;

    @Autowired
    public void setProductRepository(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public void run(String... strings) throws Exception {

        Product product1 = new Product("product 1", "description of product 1", "type", "cat1", 1);
        Product product2 = new Product("product 2", "description of product 2", "type", "cat1", 2);

        productRepository.save(product1);
        productRepository.save(product2);
    }
}
