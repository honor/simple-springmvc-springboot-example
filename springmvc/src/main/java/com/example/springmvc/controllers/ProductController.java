package com.example.springmvc.controllers;

import com.example.springmvc.Repositories.ProductRepository;
import com.example.springmvc.models.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class ProductController {

    private ProductRepository productRepository;

    @Autowired
    public void setProductRepository(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @RequestMapping(path="/")
    public String index(){
        return "index";
    }
    /*
    * GET the page, then POST for the action. See the two methods below. We first GET the addProduct page,
    * then we POST for the actual productRepository.save method's endpoint
    * observe that in addProduct.html we have a form defined as ... method="POST" th:action="@{/products/record-product}" ...
    * */
    @RequestMapping(path="/products/add", method = RequestMethod.GET)
    public String createProduct(Model model){
        model.addAttribute("product", new Product());
        return "addProduct";
    }
    @RequestMapping(path="/products/record-product", method = RequestMethod.POST)
    public String saveProduct(Product product){
        productRepository.save(product);
        return "redirect:/";
    }
    @RequestMapping(path="/products", method = RequestMethod.GET)
    public String listProducts(Model model){
        List<Product> products = productRepository.findAll();
        model.addAttribute("products", products);
        return "products";
    }
    /*
    * We can use the same page for editing as we used for adding a new product
    * */
    @RequestMapping(path="/products/edit/{id}", method = RequestMethod.GET)
    public String editProduct(Model model, @PathVariable(value="id") int id){
        model.addAttribute("product", productRepository.findOne(id));
        return "addProduct";
    }
    /*
    * see different uses of path variable here:
    * http://www.logicbig.com/tutorials/spring-framework/spring-web-mvc/spring-path-variable/
    * */
    @RequestMapping(path="/products/delete/{id}")
    public String deleteProduct(@PathVariable(name="id") int id){
        productRepository.delete(id);
        return "redirect:/products";
    }
}
